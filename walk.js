// Программа должна вернуться в точку начала движения
//n - North
//s - South
//w - West
//e - East

//n = +1
//s = -1
//w = -1
//e = +1
let array = ["n", "s", "n", "s", "n", "s", "n", "s", "n", "s"];
let count = 0;
let result = "";

function isValidWalk(walk) {
  if (walk.length > 10 || walk.length < 10) {
    return false;
  } else {
    for (let i = 0; i < walk.length; i++) {
      switch (walk[i]) {
        case "n" || "e":
          count += 1;
          break;
        case "s" || "w":
          count -= 1;
          break;
        default:
          console.log("Неверный символ");
          return false;
      }
    }
  }
  if (count == 0) {
    return true;
  } else {
    return false;
  }
}
console.log(isValidWalk(array));
console.log(isValidWalk(["n", "s", "n", "s", "n", "s", "n", "s", "n", "s"]));
console.log(
  isValidWalk(["w", "e", "w", "e", "w", "e", "w", "e", "w", "e", "w", "e"])
);
console.log(isValidWalk(["w"]));
console.log(isValidWalk(["n", "n", "n", "s", "n", "s", "n", "s", "n", "s"]));
