// Вывод наименьшего и наибольшего числа

function highAndLow(numbers) {
  let arr = numbers.split(" ");
  let highest = arr[0];
  let lowest = arr[0];

  for (let i = 0; i < arr.length - 1; i++) {
    highest = Math.max(highest, arr[i + 1]);
    lowest = Math.min(lowest, arr[i + 1]);
  }

  let result = highest + " " + lowest;

  console.log(result);

  return result;
}


highAndLow("1 2 3 4 5"); // return "5 1"
highAndLow("4 5 29 54 4 0 -214 542 -64 1 -3 6 -6");
highAndLow("1 2 -3 4 5"); // return "5 -3"
highAndLow("1 9 3 4 -5"); // return "9 -5"
