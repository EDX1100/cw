//Поиск уникального числа

var array = [0, 1, 0];
var result = {};
let unique = 0;

function findUniq(arr) {
  // Подсчёт количества повторений
  arr.forEach(function (i) {
    result[i] = result[i] + 1 || 1;
  });

  // Поиск уникального числа
  for (var key in result) {
    if (result[key] == 1) {
      unique = key;
    }
  }
  return console.log(Number(unique));
}

findUniq(array);
